---
title: "Exploring United Nations Refugee & Asylum Data With the *untools* Package"

date: "`r Sys.Date()`"

author: 
- name: Joshua Brinks
  email: jbrinks@isciences.com
  affiliation: ISciences, LLC

vignette-highlights:
- The untools package provides functions to easily acquire and visualize UN refugee and asylum seeker time series data.
- untools also provides convenience functions for fixing country name typos, removing stateless entries, and introducing common country codes.

related-packages:
- untools

related-datasets:
- "The United Nations High Commissioner for Refugees Populations of Concern: Time Series"

data-presentation-forms:
- tabular
- time series
- dyadic

output: 
  danteSubmit::danteVignette

link-citations: true
---

```{r include=FALSE}
# Load packages for creating markdown docs and nice tables
library(kableExtra)
library(knitr)
```


# Introduction

The Office of the United Nations High Commissioner for Refugees (UNHCR) provides several data sets describing annual movements of populations of concern. These include asylum seekers, refugees, internally displaced persons (IDP), returned refugees, returned IDP, and stateless persons. The [Population Statistics](http://popstats.unhcr.org/en/overview) web portal serves as a central hub for several data sets summarizing the aforementioned populations by year, month, gender, age, origin, and destination. Today we will be summarizing and exploring the [Time Series](http://popstats.unhcr.org/en/time_series/https://goo.gl/VNuKbU) dataset. This dataset consists of annual dyadic flows for all populations of concern between countries of origin (citizenship) and countries of destination (asylum/residency). Although the earliest years of record are 1951, exhibit caution when performin analysis and causal inference for years prior to 1990.

# Exploring the Data 

The Populations of Concern dataset can be acquired directly using the `getunref()` function from the `untools` package. You can install the current release of `untools` from GitLab with the `devtools` package.

```{r, eval=FALSE}
devtools::install_gitlab("/dante-sttr/untools")
```


In addition to the `unref()` function, the dataset is available for download [here](http://popstats.unhcr.org/en/time_series). The Time Series dataset is one of the cleanest data sets provided by the UNHCR, however, it might be challenging for a beginning programmer. Moreover, the country and territory names are not matched up with any country code scheme that would facilitate easy merging with additional data sets. Under the DANTE project, we have developed some tools to streamline the processing and visualization of the Time Series dataset.

Once you've acquired the data, assign it an object and view the structure. 

```{r}
library('data.table')
library('untools')
unhcr.ts<-getunref()
```
```{r echo=FALSE}
kable(unhcr.ts[1:5])
```


It is a fairly simple dataset, consisting of year, country of origin, destination country, population type, and number of migrants, but some of the naming conventions are cumbersome for an analysis environment. They contain special characters, mixed capitalization, and long titles. 

```{r}
names(unhcr.ts)
```

The descriptions of populations of concern have similar annoyances:

```{r}
unique(unhcr.ts$`Population type`)
```

# *untools* Functions for UNHCR Data
## The *prepunref* Function

The `untools` package provides several functions for processing and visualizing UNHCR data. The `prepunref()` function will help process raw UNHCR time series data by simplifying the naming structures, converting to wide or long form, selcting years of specific interest, selecting effected populations of interest, summing across groups, and rectifying country codes with ISO standards. Using `prepunref()` with no additional parameters simplifies the naming structure for column headings population types, and  adds columns for ISO3 character country codes.

```{r}
unhcr.ts.dante<-prepunref(unhcr.ts)
```
```{r echo=FALSE}
kable(unhcr.ts.dante[1:5])
```


There are several records with `Various/Unknown` as the country of origin. While these are not trivial, for this analysis we will focus on known dyadic flows between countries.

```{r}
unhcr.ts.dante<-unhcr.ts.dante[!(origin=='Various/Unknown')]
```
```{r echo=FALSE}
kable(unhcr.ts.dante[1:5])
```

By default, `prepunref()` selects all years and all affected populations, but the user can specify populations and years of interest by using the `groups` and `range` options. For example, specifying `groups = c('asylum', 'ref')` and `range = c(2000,2017)` will only process refugees between 2000 and 2017.

```{r}
unhcr.ts.dante<-prepunref(unhcr.ts, groups = c('ref'), range = c(2000, 2017))
```

Lastly, `prepunref()` provides 2 additional logical switches; `wide` and `sum`. By default, `prepunref()` returns long data frames. This is most conventient for plotting and modeling, however, sometimes it's interesting to explore data in wide form; especially time series data sets. Moreover, the `sum_groups` option will aggregate the totals across all  specified groups. Lets use these 2 switches to look at the sum of Syrian refugee and asylum seeking out-flows to Germany between 2014-2017 using `wide = TRUE`.

```{r}
unhcr.ts.dante<-prepunref(unhcr.ts, groups = c('ref', 'asylum'), range = c(2014, 2017), sum_groups=TRUE, wide=TRUE)
```
```{r echo = FALSE}
kable(unhcr.ts.dante[origin_iso=='SYR' & destination_iso=='DEU'])
```

## Static Grouped Flows

With more than 100,000 unique country-country-year records, outflows, inflows, and varying populations of interest, visualizing the UNHCR can be overwhelming. The `untools` packages provides multiple default plotting functions objects produced by the `prepunref()` function. An easy launching point to investigate flows between countries are static barplots of dyadic flows in or out of a target country. Using `plot()` on an object produced by `prepunref()` with `sum_groups = TRUE` will produce a barplot for the target country and the top 8 destination or origin countries. The user specifies the country of interest, a year of interest, and whether they want to view inflows (`mode = 'in'`) or outflows (`mode = 'out'`). Let's start by viewing asylum seeking inflows to the United States in 2013.

```{r dev='png'}
unhcr.ts.dante<-prepunref(unhcr.ts, groups = c('asylum'), range = c(2012, 2017))
usa.in<-plot(unhcr.ts.dante, country = 'USA', mode = 'in', yr = 2013)
```

Somewhat surprisingly, China tops the list, while Central America rounds out the rest of the top 5. By default, `plot()` will list up to 8 countries using the maximum year in the dataset (currently 2017). Similarly, we can view asylum seeking outflows from the Philippines in 2017. 

```{r dev='png'}
unhcr.ts.dante<-prepunref(unhcr.ts, groups = c('asylum'), range = c(2012, 2017))
phl.out<-plot(unhcr.ts.dante, country = 'PHL', mode = 'out')

```

## Stacked Static Flows by Population Type

Up until this point we've visualized cumulative migrant flows across all groups or a singular group, but it may be of interest to examine relative proportions of asylum seekers, refugees, and stateless persons. The `untools` package provides default plotting functions to visualize stacked bar charts of migrant inflows or outflows by groups. Let's re-examine inflows of migrants to the USA in 2017, but this time include breakdowns by type. To maintain effected population breakdowns specify `sum_groups = FALSE`.

```{r dev='png'}
unhcr.stacked<-prepunref(unhcr.ts, groups = c('asylum', 'ref','stateless'), range = c(2000, 2017), sum_groups = FALSE)
usa.stacked.in<-plot(unhcr.stacked, country = 'USA', mode = 'in')
```

## Plotting Time Series

Although static plots of migrant flows are interesting, it's often more illuminating to examing time series data for migrant inflows and outflows. The `untools` package also provides default plotting functions to visualize time series migrant flows for data frames produced with the `prepunref()` using `sum_groups = TRUE`. The default plotting function will produce a plot for all years present in the raw data using the 5 countries with the highest totals in the maximum year of the dataset. Let's view annual cumulative refugee and asylum seeking in-flows to the USA from 2000-2017.

```{r dev='png'}
unhcr.ts.dante<-prepunref(unhcr.ts, groups = c('asylum', 'ref'), range = c(2000, 2017), sum_groups = TRUE)
usa.ts.in<-plot(unhcr.ts.dante, country = 'USA', mode = 'in')
```

Lastly, similar to the static default plotting functions, we can specify `mode = 'out'` to view outflows from a given country.

```{r dev='png'}
phl.ts.out<-plot(unhcr.ts.dante, country = 'PHL', mode = 'out')
```


```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```